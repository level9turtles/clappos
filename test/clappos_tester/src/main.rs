use clappos::Clappos;
use std::process::exit;

fn main() {

    let args = Args::parse();
    println!("have arg1: {:?}", args.arg1);
    println!("have arg2: {:?}", args.arg2);
    println!("have arg3: {:?}", args.arg3);
    println!("have arg4: {:?}", args.arg4);
    println!("have arg5: {:?}", args.arg5);

    /*
    let args2 = Args2::parse();
    println!("{}", args2.env)
     */
}

///test can i see the docs?
///  Test that we don't mess up struct name parsing
///Test
#[derive(Clappos)]
pub struct Args {
    /// info about arg1
    arg1: String,
    /// info: on, arg2
    arg2: i32,
    arg3: bool,
    /// ok yeah must have arg comments for now
    arg4: Option<i32>,
    /*
        this doesn't show up
     */
    // does this? nope.
    /// info
    /// for arg5
    arg5: Option<String>
}

/*
#[derive(Clappos)]
struct Args2 {
    env: String
}

 */