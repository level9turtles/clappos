# Clappos

## Description
Clappos is a simple, flagless alternative to Clap for building clean, simple CLI tools.

## Project status
This project is being actively developed and maintained by the Level9Turtles organization. For contribution information, 
or any other concerns/questions, please reach out to level9turtles.HAL@gmail.com
## Usage

### Installation
Add the following to your Cargo.toml
```TOML
clappos = "*"
```
or run
```bash
cargo add clappos
```
to get the latest version

### Examples

```Rust
use clappos::Clappos;

#[derive(Clappos)]
struct Args {
    /// help menu info for arg1
    string_arg: String,
    /// help menu info for arg2
    num_arg: i32,
    /// help menu info for arg3
    bool_arg: bool,
    /// help menu info for arg4
    op_string_arg: Option<String>,
    /// help menu info for arg5
    op_num_arg: Option<i32>
}

fn main() {
    let args = Args::parse(); // user input: `cargo run this 1 true that`
    println!("{}" args.string_arg); // => "this"
    println!("{}", args.num_arg); // => 1
    println!("{}", args.bool_arg); // => true
    println!("{}", args.op_string_arg); // => Some("that")
    println!("{}", args.op_num_arg); // => None
    
}
```

There is a trade-off for flagless CLI tools, which is that any optional arguments must come last, and a Some value 
cannot come after a None value when being used. In the future, I may add a feature to flag your optional arguments.

For now though, these examples are invalid:
```Rust
#[derive(Clappos)]
struct Args {
    optional_number: Option<i32>,
    required_string: String,      // this won't work because require arg comes after optional arg
}

#[derive(Clappos)]
struct Args {
    string_arg: String,
    op_num1: Option<i32>,        // this is valid, but currently a user could not pass op_num2 
    op_num2: Option<i32>,        // without also passing an op_num1. May fix this with flags in the future
          
}
```

## Authors and acknowledgment
This tool was written and is maintained by Adam Horn of the Level9Turtles High Altitude Lab. For questions and concerns
please email me at level9turtles.HAL@gmail.com

## License
This project is licensed under the MIT open source license.
